package main

import "fmt"
import "runtime"


func getAverage(numbers []int, ch chan float64) {
    var sum = 0
    for _, e := range numbers {
        sum += e
    }
    ch <- float64(sum) / float64(len(numbers))
}

func sum(array []int) int {  
	result := 0  
	for _, v := range array {  
	 result += v  
	}  
	return result  
   }  

   func selectionSort(arr []int) {
	for i := 0; i < len(arr)-1; i++ {
	  minIndex := i
	  for j := i + 1; j < len(arr); j++ {
		if arr[minIndex] > arr[j] {
		  minIndex = j
		}
	  }
	  tmp := arr[i]
	  arr[i] = arr[minIndex]
	  arr[minIndex] = tmp
	}
  }

func main() {
    runtime.GOMAXPROCS(2)
	var cities [3]string
	cities[0] = "balikpapan"
	cities[1] = "sleman"
	cities[2] = "surabaya"

	fmt.Println(cities)
	fmt.Println(cities[0])

	var numbers = []int{2,4,6,8,10,12,14,16,28 }

	var getMinMax = func(n []int) (int, int) {
        var min, max int
        for i, e := range n {
            switch {
            case i == 0:
                max, min = e, e
            case e > max:
                max = e
            case e < min:
                min = e
            }
        }
        return min, max
    }
	var kelompok_1 = []int{2,4,6}
	var kelompok_2 = []int{8,10,12}
	var kelompok_3 = []int{14,16,28}
	fmt.Println(numbers)
	fmt.Println("kelompok 1 = ", kelompok_1)
	// fmt.Print("jumlah :", sum(kelompok_1))  
	n := 3
	sum := 0
	for i := 0; i < n; i++ { 
        sum += (kelompok_1[i]) 
	} 
	avg := (float64(sum)) / (float64(n)) 
	fmt.Println("Sum = ", sum, "\nAverage = ", avg)
	var min, max = getMinMax(kelompok_1)
	fmt.Println("min = ", min)
	fmt.Println("max = ", max)

	selectionSort(kelompok_1)
  for i := 0; i < len(kelompok_1); i++ {
    fmt.Print(kelompok_1[i], " ")
  }
	  fmt.Print("= sort data 1")

	fmt.Println("- - - - - - - - - - - - - - - - -")
	fmt.Println("kelompok 2 = ", kelompok_2)
	// fmt.Print("jumlah :", sum(kelompok_1))  
	for i := 0; i < n; i++ { 
        sum += (kelompok_2[i]) 
	} 
	avg_2 := (float64(sum)) / (float64(n)) 
	fmt.Println("Sum = ", sum, "\nAverage = ", avg_2)
	var min_2, max_2 = getMinMax(kelompok_2)
	fmt.Println("min = ", min_2)
	fmt.Println("max = ", max_2)

	selectionSort(kelompok_2)
 	for i := 0; i < len(kelompok_2); i++ {
    fmt.Print(kelompok_2[i], " ")
  	}
	  fmt.Print("= sort data 2")
	  
	fmt.Println("- - - - - - - - - - - - - - - - -")
	fmt.Println("kelompok3 = ", kelompok_3)
	// fmt.Print("jumlah :", sum(kelompok_1))  
	for i := 0; i < n; i++ { 
        sum += (kelompok_3[i]) 
	} 
	avg_3 := (float64(sum)) / (float64(n)) 
	fmt.Println("Sum = ", sum, "\nAverage = ", avg_3)
	var min_3, max_3 = getMinMax(kelompok_3)
	fmt.Println("min = ", min_3)
	fmt.Println("max = ", max_3)

	selectionSort(kelompok_3)
  for i := 0; i < len(kelompok_3); i++ {
    fmt.Print(kelompok_3[i], " ")
  	}
	fmt.Print("= sort data 3")
	fmt.Println("- - - - - - - - - - - - - - - - -")
	  
}