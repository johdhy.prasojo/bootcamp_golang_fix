package main

import (
	"tugas_10/config"
	"tugas_10/controller"
	"tugas_10/model"

	"github.com/gin-gonic/gin"
)

func main() {

	//conection to DB Postgree
	dbPG := config.Connect()
	strDB := controller.StrDB{DB: dbPG}

	//migration to DB Postgree
	model.Migration(dbPG)

	//seeder to DB Postgree
	model.SeederProv(dbPG)
	model.SeederDistrict(dbPG)
	model.SeederSubDistrict(dbPG)

	r := gin.Default()
	provinces := r.Group("/province")
	provinces.POST("", strDB.PostAddProvince)
	provinces.GET("", strDB.GetListProvince)
	provinces.PUT("/:id", strDB.PutUpdateProvince)
	provinces.DELETE("/:id", strDB.PutUpdateProvince)

	r.GET("/province-all", strDB.GetListProvinceDistrictSubDistrict)
	r.GET("/province-restructur", strDB.GetListProvinceDistrictSubDistrictRename)
	r.GET("/district-province", strDB.GetListDistrcitProvince)
	r.GET("/district", strDB.GetListDistrcit)

	districts := r.Group("/district")
	districts.POST("", strDB.PostAddDistrict)
	// districts.GET("", strDB.GetDistrictProvince)

	r.Run(":1234")
}

// baseURL = "http://api/icndb.com"

// type valueJoke struct {
// 	type string
// 	value [] type struct {
// 		id string
// 		joke string
// 	}
// }

// func fetchJokes() ([]valueJoke, error) {
// 	var client = &http.Client{}
// 	var data []valueJoke

// 	request, err := http.NewRequest("GET", baseURL+"/jokes/4", nil)
// 	if err != nil {
// 		fmt.Println("Error Request", err.Error())
// 		return nil, err
// 	}

// 	response, err := client.Do(request)
// 	if err != nil {
// 		fmt.Println("Error Request", err.Error())
// 		return nil, err
// 	}

// 	defer response.Body.Close()
// 	fmt.Println(response.Body)
// 	err = json.NewDecoder(response.Body).Decode(&data)
// 	if err != nil {
// 		fmt.Println("Error Request", err.Error())
// 		return nil, err
// 	}

// 	return data, nil
// }
