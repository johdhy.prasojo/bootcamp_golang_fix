package model

import (
	"fmt"

	"gorm.io/gorm"
)

//migration
func Migration(db *gorm.DB) {
	db.Migrator().DropTable(&SubDistricts{})
	db.Migrator().DropTable(&Districts{})
	db.Migrator().DropTable(&Provinces{})

	if check := db.Migrator().HasTable(&Provinces{}); !check {
		db.Migrator().CreateTable(&Provinces{})
		fmt.Println("create table provinces")
	}
	if check := db.Migrator().HasTable(&Districts{}); !check {
		db.Migrator().CreateTable(&Districts{})
		fmt.Println("create table districts")
	}
	if check := db.Migrator().HasTable(&SubDistricts{}); !check {
		db.Migrator().CreateTable(&SubDistricts{})
		fmt.Println("create table sub disctricts")
	}
}

// // districts
// func MigrationDistricts(db *gorm.DB) {
// 	var check bool
// 	check = db.Migrator().HasTable(&Districts{})
// 	if !check {
// 		db.Migrator().CreateTable(&Districts{})
// 		fmt.Println("create table Districts")
// 	}
// }

// // sub districts
// func MigrationSubDistricts(db *gorm.DB) {
// 	var check bool
// 	check = db.Migrator().HasTable(&Sub_Districts{})
// 	if !check {
// 		db.Migrator().CreateTable(&Sub_Districts{})
// 		fmt.Println("create table Sub_Districts")
// 	}
// }

// //persons
// func MigrationPersons(db *gorm.DB) {
// 	var check bool
// 	check = db.Migrator().HasTable(&Persons{})
// 	if !check {
// 		db.Migrator().CreateTable(&Persons{})
// 		fmt.Println("create table Persons")
// 	}
// }
