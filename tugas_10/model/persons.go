package model

import "gorm.io/gorm"

//migration persons
type Persons struct {
	gorm.Model
	Sub_District_id int
	Nip             string
	Full_Name       string
	Firts_Name      string
	Last_Name       string
	Birth_Date      string
	Birth_Place     string
	Gender          bool
	Zona_Location   string
}
