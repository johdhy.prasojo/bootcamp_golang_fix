package model

import "gorm.io/gorm"

//Districts
type Districts struct {
	Name string
	gorm.Model
	ProvinceID   uint
	SubDistricts []SubDistricts `gorm:"foreignKey:DistrictID"`
}
