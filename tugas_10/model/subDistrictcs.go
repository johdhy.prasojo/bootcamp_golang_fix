package model

import "gorm.io/gorm"

//SubDistricts sample
type SubDistricts struct {
	Name string
	gorm.Model
	DistrictID uint
}
