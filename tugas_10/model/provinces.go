package model

import (
	"gorm.io/gorm"
)

//migration provinces
type Provinces struct {
	gorm.Model
	Name      string      `json:"name"`
	Districts []Districts `gorm:"foreignKey:ProvinceID" json:"districts"`
}

// //migration districs
// type Districts struct {
// 	gorm.Model
// 	Name         string
// 	Id_Provinces int
// }

// //migration sub_district
// type Sub_Districts struct {
// 	gorm.Model
// 	Name        string
// 	District_id int
// }

// //migration persons
// type Persons struct {
// 	gorm.Model
// 	Sub_District_id int
// 	Nip             string
// 	Full_Name       string
// 	Firts_Name      string
// 	Last_Name       string
// 	Birth_Date      string
// 	Birth_Place     string
// 	Gender          bool
// 	Zona_Location   string
// }
