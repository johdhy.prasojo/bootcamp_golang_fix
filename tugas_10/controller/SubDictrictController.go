package controller

import (
	"fmt"
	"net/http"
	"tugas_10/model"

	"github.com/gin-gonic/gin"
)

//PostAddProvince to DB Postgree
func (strDB *StrDB) PostSubDistrict(c *gin.Context) {
	var (
		subDistrict model.SubDistricts
		// province model.Provinces
		result gin.H
	)

	// getId := strDB.DB.First(&province)
	// getId.id
	// getId.Error

	// name := c.PostForm("name")
	// idProvinsi := c.PostForm(getId.id)
	// district.Name = name
	// district.Id_Provinces = idProvinsi

	// strDB.DB.Create(&district)
	// result = gin.H{
	// 	"result": district,
	// }
	// c.JSON(http.StatusOK, result)

	if err := c.Bind(&subDistrict); err != nil {
		fmt.Println("Tidak ada data atau terjadi kesalahan")
	} else {
		strDB.DB.Create(&subDistrict)
		result = gin.H{
			"message":   "success",
			"name":      subDistrict.Name,
			"districts": subDistrict.DistrictID,
			"data": map[string]interface{}{
				"id":           subDistrict.ID,
				"name":         subDistrict.Name,
				"districts_id": subDistrict.DistrictID,
				"created_at":   subDistrict.CreatedAt,
			},
		}
	}
	c.JSON(http.StatusOK, result)
}

//get
func (strDB *StrDB) GetSubDistrict(c *gin.Context) {
	var (
		subDistrict []model.SubDistricts
		result      gin.H
	)
	strDB.DB.Find(&subDistrict)
	if length := len(subDistrict); length <= 0 {
		// result = gin.H{
		// 	"message":"success",
		// 	"data":nil,
		// 	"count":length,
		// }
		result = ResponAPINil(subDistrict, length)
	} else {
		result = ResponAPI(subDistrict, length)
	}
	c.JSON(http.StatusOK, result)
}

//put
func (strDB *StrDB) PutUpdateSubDistrict(c *gin.Context) {
	id := c.Query("id")
	name := c.PostForm("name")
	// idProvinsi := c.PostForm(getId.id)
	var (
		subDistrict    model.SubDistricts
		newsubDistrict model.SubDistricts
		result         gin.H
	)
	err := strDB.DB.First(&subDistrict, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newsubDistrict.Name = name
	err = strDB.DB.Model(&subDistrict).Updates(newsubDistrict).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

//delete
func (strDB *StrDB) DeleteSubDistricts(c *gin.Context) {
	var (
		subDistrict model.SubDistricts
		result      gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&subDistrict, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = strDB.DB.Delete(&subDistrict).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}
