package controller

import (
	"fmt"
	"net/http"
	"tugas_10/model"

	"github.com/gin-gonic/gin"
)

//PostAddProvince to DB Postgree
func (strDB *StrDB) PostAddProvince(c *gin.Context) {
	var (
		province model.Provinces
		result   gin.H
	)

	if err := c.Bind(&province); err != nil {
		fmt.Println("Tidak ada data atau terjadi kesalahan")
	} else {
		strDB.DB.Create(&province)
		result = gin.H{
			"message":  "success",
			"province": province.Name,
			"data": map[string]interface{}{
				"id":         province.ID,
				"name":       province.Name,
				"created_at": province.CreatedAt,
			},
		}
	}
	c.JSON(http.StatusOK, result)
}

//get
func (strDB *StrDB) GetListProvince(c *gin.Context) {
	var (
		province []model.Provinces
		result   gin.H
	)
	strDB.DB.Preload("Districts").Find(&province)
	if length := len(province); length <= 0 {
		// result = gin.H{
		// 	"message":"success",
		// 	"data":nil,
		// 	"count":length,
		// }
		result = ResponAPINil(province, length)
	} else {
		result = ResponAPI(province, length)
	}
	c.JSON(http.StatusOK, result)
}

// GetListProvinceDistrictSubDistrict comment
func (strDB *StrDB) GetListProvinceDistrictSubDistrict(c *gin.Context) {
	var (
		province []model.Provinces
		result   gin.H
	)

	strDB.DB.
		// Joins("JOIN districts on districts.province_id=provinces.id").
		// Joins("JOIN sub_districts on sub_districts.district_id=districts.id").
		Preload("Districts").
		Preload("Districts.SubDistricts").
		Find(&province)
	if length := len(province); length <= 0 {
		result = ResponAPINil(province, length)
	} else {
		result = ResponAPI(province, length)
	}

	c.JSON(http.StatusOK, result)
}

// GetListProvinceDistrictSubDistrictRename comment
func (strDB *StrDB) GetListProvinceDistrictSubDistrictRename(c *gin.Context) {
	type SubDistricts struct {
		Name       string `json:"name"`
		ID         uint   `gorm:"primarykey" json:"id"`
		DistrictID uint   `json:"district_id"`
	}
	type Districts struct {
		Name         string         `json:"name"`
		ID           uint           `gorm:"primarykey" json:"id"`
		ProvinceID   uint           `json:"province_id"`
		SubDistricts []SubDistricts `gorm:"foreignKey:DistrictID" json:"sub_districts"`
	}
	type Provinces struct {
		Name      string      `json:"name"`
		Districts []Districts `gorm:"foreignKey:ProvinceID" json:"districts"`
		ID        uint        `gorm:"primarykey" json:"id"`
	}

	var (
		province []Provinces
		result   gin.H
	)

	strDB.DB.
		// Joins("JOIN districts on districts.province_id=provinces.id").
		// Joins("JOIN sub_districts on sub_districts.district_id=districts.id").
		Preload("Districts").
		Preload("Districts.SubDistricts").
		Find(&province)
	if length := len(province); length <= 0 {
		result = ResponAPINil(province, length)
	} else {
		result = ResponAPI(province, length)
	}

	c.JSON(http.StatusOK, result)
}

//put
func (strDB *StrDB) PutUpdateProvince(c *gin.Context) {
	id := c.Query("id")
	name := c.PostForm("name")
	var (
		province    model.Provinces
		newProvince model.Provinces
		result      gin.H
	)
	err := strDB.DB.First(&province, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newProvince.Name = name
	err = strDB.DB.Model(&province).Updates(newProvince).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

//delete
func (strDB *StrDB) DeleteProvince(c *gin.Context) {
	var (
		province model.Provinces
		result   gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&province, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = strDB.DB.Delete(&province).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}
