package controller

import (
	"fmt"
	"net/http"
	"tugas_10/model"

	"github.com/gin-gonic/gin"
)

//PostAddProvince to DB Postgree
func (strDB *StrDB) PostAddPerson(c *gin.Context) {
	var (
		person model.Persons
		// province model.Provinces
		result gin.H
	)

	// getId := strDB.DB.First(&province)
	// getId.id
	// getId.Error

	// name := c.PostForm("name")
	// idProvinsi := c.PostForm(getId.id)
	// district.Name = name
	// district.Id_Provinces = idProvinsi

	// strDB.DB.Create(&district)
	// result = gin.H{
	// 	"result": district,
	// }
	// c.JSON(http.StatusOK, result)

	if err := c.Bind(&person); err != nil {
		fmt.Println("Tidak ada data atau terjadi kesalahan")
	} else {
		strDB.DB.Create(&person)
		result = gin.H{
			"message":       "success",
			"nip":           person.Nip,
			"full_name":     person.Full_Name,
			"first_ame":     person.Firts_Name,
			"last_name":     person.Last_Name,
			"birth_date":    person.Birth_Date,
			"birth_place":   person.Birth_Place,
			"gender":        person.Gender,
			"zona_location": person.Zona_Location,
			"data": map[string]interface{}{
				"id":              person.ID,
				"sub_district_id": person.Sub_District_id,
				"nip":             person.Nip,
				"full_name":       person.Full_Name,
				"first_ame":       person.Firts_Name,
				"last_name":       person.Last_Name,
				"birth_date":      person.Birth_Date,
				"birth_place":     person.Birth_Place,
				"gender":          person.Gender,
				"zona_location":   person.Zona_Location,
			},
		}
	}
	c.JSON(http.StatusOK, result)
}

//get
func (strDB *StrDB) GetPersons(c *gin.Context) {
	var (
		person []model.Persons
		result gin.H
	)
	strDB.DB.Find(&person)
	if length := len(person); length <= 0 {
		// result = gin.H{
		// 	"message":"success",
		// 	"data":nil,
		// 	"count":length,
		// }
		result = ResponAPINil(person, length)
	} else {
		result = ResponAPI(person, length)
	}
	c.JSON(http.StatusOK, result)
}

//put
func (strDB *StrDB) PutUpdatePersons(c *gin.Context) {
	id := c.Query("id")
	// sub_district_id := c.PostForm("sub_district_id")
	nip := c.PostForm("nip")
	full_name := c.PostForm("full_name")
	first_name := c.PostForm("first_ame")
	Last_name := c.PostForm("last_name")
	birth_date := c.PostForm("birth_date")
	birth_place := c.PostForm("birth_place")
	// gender := c.PostForm("gender")
	zona_location := c.PostForm("zona_location")
	var (
		person    model.Persons
		newPerson model.Persons
		result    gin.H
	)
	err := strDB.DB.First(&person, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	// newPerson.Sub_District_id = sub_district_id
	newPerson.Nip = nip
	newPerson.Full_Name = full_name
	newPerson.Firts_Name = first_name
	// newPerson.Last_Nameme = last_name
	newPerson.Birth_Date = birth_date
	newPerson.Birth_Place = birth_place
	// newPerson.Gender = gender
	newPerson.Zona_Location = zona_location
	err = strDB.DB.Model(&person).Updates(newPerson).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

//delete
func (strDB *StrDB) DeletePersons(c *gin.Context) {
	var (
		person model.Persons
		result gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&person, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = strDB.DB.Delete(&person).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}
