package controller

import "github.com/gin-gonic/gin"

//responseAPI
func ResponAPI(str interface{}, length int) gin.H {
	return gin.H{
		"message": "success",
		"data":    nil,
		"count":   length,
	}
}

//nil
func ResponAPINil(str interface{}, length int) gin.H {
	return gin.H{
		"message": "success",
		"data":    nil,
		"count":   length,
	}
}
