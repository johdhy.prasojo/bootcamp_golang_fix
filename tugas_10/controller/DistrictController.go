package controller

import (
	"fmt"
	"net/http"
	"tugas_10/model"

	"github.com/gin-gonic/gin"
)

//PostAddProvince to DB Postgree
func (strDB *StrDB) PostAddDistrict(c *gin.Context) {
	var (
		district model.Districts
		// province model.Provinces
		result gin.H
	)

	// getId := strDB.DB.First(&province)
	// getId.id
	// getId.Error

	// name := c.PostForm("name")
	// idProvinsi := c.PostForm(getId.id)
	// district.Name = name
	// district.Id_Provinces = idProvinsi

	// strDB.DB.Create(&district)
	// result = gin.H{
	// 	"result": district,
	// }
	// c.JSON(http.StatusOK, result)

	if err := c.Bind(&district); err != nil {
		fmt.Println("Tidak ada data atau terjadi kesalahan")
	} else {
		strDB.DB.Create(&district)
		result = gin.H{
			"message":     "success",
			"name":        district.Name,
			"id_provinsi": district.ProvinceID,
			"data": map[string]interface{}{
				"id":          district.ID,
				"name":        district.Name,
				"id_provinsi": district.ProvinceID,
				"created_at":  district.CreatedAt,
			},
		}
	}
	c.JSON(http.StatusOK, result)
}

// GetListDistrcit comment
func (strDB *StrDB) GetListDistrcit(c *gin.Context) {

	type data struct {
		ID       string
		District string
		Province string
	}
	var (
		district     model.Districts
		result       gin.H
		dataResponse []data
	)

	strDB.DB.Model(&district).Select("districts.id, districts.name as district, provinces.name as province").
		Joins("join provinces on provinces.id = districts.province_id").
		Scan(&dataResponse)

	if length := len(dataResponse); length <= 0 {
		result = ResponAPINil(dataResponse, length)
	} else {
		result = ResponAPI(dataResponse, length)
	}

	c.JSON(http.StatusOK, result)
}

// //get
// func (strDB *StrDB) GetDistrict(c *gin.Context) {
// 	var (
// 		district []model.Districts
// 		result   gin.H
// 	)
// 	strDB.DB.Find(&district)
// 	if length := len(district); length <= 0 {
// 		// result = gin.H{
// 		// 	"message":"success",
// 		// 	"data":nil,
// 		// 	"count":length,
// 		// }
// 		result = ResponAPINil(district, length)
// 	} else {
// 		result = ResponAPI(district, length)
// 	}
// 	c.JSON(http.StatusOK, result)
// }

// GetListDistrcitProvince comment
func (strDB *StrDB) GetListDistrcitProvince(c *gin.Context) {
	type Provinces struct {
		Name string `json:"name"`
		ID   uint   `gorm:"primarykey" json:"id"`
	}
	type Districts struct {
		Name       string      `json:"name"`
		ID         uint        `gorm:"primarykey" json:"id"`
		ProvinceID uint        `json:"province_id"`
		Provinces  []Provinces `gorm:"foreignKey:ProvinceID" json:"provinces"`
	}

	var (
		district []Districts
		result   gin.H
	)

	strDB.DB.
		// Joins("JOIN districts on districts.province_id=provinces.id").
		// Joins("JOIN sub_districts on sub_districts.district_id=districts.id").
		Preload("Provinces").
		Find(&district)

	if length := len(district); length <= 0 {
		result = ResponAPINil(district, length)
	} else {
		result = ResponAPI(district, length)
	}

	c.JSON(http.StatusOK, result)
}

//put
func (strDB *StrDB) PutUpdateDistrict(c *gin.Context) {
	id := c.Query("id")
	name := c.PostForm("name")
	// idProvinsi := c.PostForm(getId.id)
	var (
		district    model.Districts
		newDistrict model.Districts
		result      gin.H
	)
	err := strDB.DB.First(&district, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newDistrict.Name = name
	err = strDB.DB.Model(&district).Updates(newDistrict).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

//delete
func (strDB *StrDB) DeleteDistricts(c *gin.Context) {
	var (
		district model.Districts
		result   gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&district, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = strDB.DB.Delete(&district).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}
