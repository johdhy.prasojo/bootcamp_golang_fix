package main

import (
	"fmt"
	"runtime"
)

func ShowData(maxIndex int, message string) {
	for i := 0; i < maxIndex; i++ {
		fmt.Println((i + 1), message)
	}
}

//data
func mains() {
	runtime.GOMAXPROCS(1)

	go ShowData(100, "dengan Gorutine")
	ShowData(100, "tanpa Goroutine")

	var input string
	fmt.Scanln(&input)

}
