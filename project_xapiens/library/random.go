package library

import (
	"fmt"
	"math/rand"
	"strconv"
)

// percobaan structs
type Hasil struct {
	angka []int
}

// random fungsi
func Random() {
	// var Total int
	// var Rata2 int
	// var cobaHasill HasilRandom
	var kelompok [200]int
	for i := 0; i < 200; {
		kelompok[i] = rand.Intn(200)
		i++
	}
	fmt.Println("Banyak Nilai :", kelompok)
	Totals := kelompok[0:200]
	fmt.Println("Total Nilai : " + strconv.Itoa(Total(Totals)))
	fmt.Println("Rata-rata Nilai : " + strconv.Itoa(Rata2(Totals)))
	// fmt.Println("Min Kelompok : " + strconv.Itoa(Min(Totals)))
	// fmt.Println("Max Kelompok : " + strconv.Itoa(Max(Totals)))
	fmt.Println("===============================================")
	Kelompok_1 := kelompok[0:40]
	fmt.Println("Banyak Nilai Kelompok_1 :", Kelompok_1)
	fmt.Println("Total Nilai Kelompok_1 : " + strconv.Itoa(Total(Kelompok_1)))
	fmt.Println("Rata-rata Nilai Kelompok_1 : " + strconv.Itoa(Rata2(Kelompok_1)))
	fmt.Println("===============================================")
	Kelompok_2 := kelompok[40:80]
	fmt.Println("Banyak Nilai Kelompok_2 :", Kelompok_2)
	fmt.Println("Total Nilai Kelompok_1 : " + strconv.Itoa(Total(Kelompok_2)))
	fmt.Println("Rata-rata Nilai Kelompok_1 : " + strconv.Itoa(Rata2(Kelompok_2)))
	fmt.Println("===============================================")
	Kelompok_3 := kelompok[80:120]
	fmt.Println("Banyak Nilai Kelompok_3 :", Kelompok_3)
	fmt.Println("Total Nilai Kelompok_1 : " + strconv.Itoa(Total(Kelompok_3)))
	fmt.Println("Rata-rata Nilai Kelompok_1 : " + strconv.Itoa(Rata2(Kelompok_3)))
	fmt.Println("===============================================")
	Kelompok_4 := kelompok[120:160]
	fmt.Println("Banyak Nilai Kelompok_4 :", Kelompok_4)
	fmt.Println("Total Nilai Kelompok_1 : " + strconv.Itoa(Total(Kelompok_4)))
	fmt.Println("Rata-rata Nilai Kelompok_1 : " + strconv.Itoa(Rata2(Kelompok_4)))
	fmt.Println("===============================================")
	Kelompok_5 := kelompok[160:200]
	fmt.Println("Banyak Nilai Kelompok_5 :", Kelompok_5)
	fmt.Println("Total Nilai Kelompok_1 : " + strconv.Itoa(Total(Kelompok_5)))
	fmt.Println("Rata-rata Nilai Kelompok_1 : " + strconv.Itoa(Rata2(Kelompok_5)))
	fmt.Println("===============================================")
}

//coba struct type method
// func (cobaHasill HasilRandom) Total2() {
// 	fmt.Println("ini menggunakan struct type method")
// }
//nilai random
// func inisialKelompok(min int, max int, maxNilai int) (nilai []int) {
// 	for i := 1; i <= max; i++ { //membuat 100 random nilai
// 		nilai = append(nilai, rand.Intn(maxNilai-min)+min)
// 	}
// 	fmt.Println(nilai)
// 	return
// }
//hasil random
// func Apa() {
// 	fmt.Println("Banyak Nilai :", Santai)
// }
// func randomArray(len int) []int {
// 	a := make([]int, len)
// 	for i := 0; i <= len-1; i++ {
// 		a[i] = rand.Intn(len)
// 	}
// 	return a
// }
// // Santai
// func Santai(len int) []int {
// 	var kelompok [200]int
// 	for i := 0; i < 200; {
// 		kelompok[i] = rand.Intn(200)
// 		i++
// 	}
// }
