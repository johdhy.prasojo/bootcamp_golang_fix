package library

import (
	"testing"
)

var (
	volumeSeharusnya float64 = 125
	luasSeharusnya   float64 = 25
)

// hitung volume
func TestHitungVolume(t *testing.T) {

	t.Logf("volume : %.2f", KubusVolume(5))

	if KubusVolume(5) != volumeSeharusnya {
		t.Errorf("salah seharusnya: %.2f", volumeSeharusnya)
	}
}

// hitung luas
func TestHitungLuas(t *testing.T) {

	t.Logf("luas : %.2f", LuasPersegi(5))

	if LuasPersegi(5) != volumeSeharusnya {
		t.Errorf("salah seharusnya: %.2f", volumeSeharusnya)
	}
}

func TestHitungLuasVolume(t *testing.T) {

	t.Logf("volume : %.2f", LuasPersegi(5))

	if KubusVolume(5) != volumeSeharusnya {
		t.Errorf("salah seharusnya: %.2f", volumeSeharusnya)
	}

	t.Logf("luas : %.2f", LuasPersegi(5))

	if LuasPersegi(5) != volumeSeharusnya {
		t.Errorf("salah seharusnya: %.2f", volumeSeharusnya)
	}
}
