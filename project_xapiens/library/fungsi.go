package library

// "strconv"

// percobaa struct
type NilaiRandom struct {
	Total, Rata2, Min, Max int
}

//fungsi Min
func Min(kelompok []int) int {
	var minimum = 0
	for i := 0; i < len(kelompok); i++ {
		if minimum >= kelompok[i] {
			minimum = kelompok[i]
		}
	}
	return minimum
}

//fungsi Max
func Max(kelompok []int) int {
	var maximum = 0
	for i := 0; i < len(kelompok); i++ {
		if maximum <= kelompok[i] {
			maximum = kelompok[i]
		}
	}

	return maximum
}

//fungsi Total
func Total(kelompok []int) int {
	sum := 0
	for i := 0; i < len(kelompok); i++ {
		sum += (kelompok[i])
	}

	return sum
}

// rata-rata kelompok
func Rata2(kelompok []int) int {
	var sum int = Total(kelompok)

	return sum / len(kelompok)
}
