package model

import (
	"gorm.io/gorm"
)

//migration provinces
type Provinces struct {
	gorm.Model
	Name string
}

//migration districs
type Districts struct {
	gorm.Model
	Name         string
	Id_Provinces int
}
