package model

import (
	"fmt"

	"gorm.io/gorm"
)

//migration
func Migration(db *gorm.DB) {
	var check bool
	check = db.Migrator().HasTable(&Provinces{})
	if !check {
		db.Migrator().CreateTable(&Provinces{})
		fmt.Println("create table provinces")
	}
}

func MigrationDistricts(db *gorm.DB) {
	var check bool
	check = db.Migrator().HasTable(&Districts{})
	if !check {
		db.Migrator().CreateTable(&Districts{})
		fmt.Println("create table Districts")
	}
}
