package structs

//struct
type Belanjaan struct {
	Id          int     `json: "id"`
	Item        string  `json: "nama_item"`
	Quantity    int     `json: "jumlah_item"`
	HargaSatuan float64 `json: "hargaSatuan"`
}

//list data
type BelanjaanList struct {
	belanjaan []Belanjaan
}
