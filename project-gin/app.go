package main

import (
	"project/config"
	"project/controller"
	"project/model"

	"github.com/gin-gonic/gin"
)

// // struct
// type Belanjaan struct {
// 	Item        string  `json: "nama_item"`
// 	Quantity    int     `json: "jumlah_item"`
// 	HargaSatuan float64 `json: "hargaSatuan"`
// }

// //list data
// type BelanjaanList struct {
// 	belanjaStruct []Belanjaan
// }

func main() {

	//conection to DB Postgree
	dbPG := config.Connect()
	strDB := controller.StrDB{DB: dbPG}
	//migration to DB Postgree
	model.Migration(dbPG)
	model.MigrationDistricts(dbPG)

	r := gin.Default()
	provinces := r.Group("/province")
	provinces.POST("", strDB.PostAddProvince)
	provinces.GET("", strDB.GetListProvince)
	provinces.PUT("/:id", strDB.PutUpdateProvince)
	provinces.DELETE("/:id", strDB.PutUpdateProvince)

	districts := r.Group("/district")
	districts.POST("/district", strDB.PostAddDistrict)
	districts.GET("/district", strDB.GetDistrictProvince)
	// v1 := r.Group("api")
	// {
	// 	v1.POST("/belanjaan", listbelanjaan.PostBelanjaan)
	// 	v1.PUT("/belanjaan", listbelanjaan.PutUpdatePurchase)
	// 	// v1.GET("/belanjaan", controller.GetData)
	// 	v1.GET("/belanjaan", listbelanjaan.GetBelanjaan)
	// 	v1.GET("//belanjaan-total", listbelanjaan.GetTotalPurchase)
	// }

	r.Run(":3000") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

//Post
// func (listbelanjaan *BelanjaanList) PostBelanjaan(c *gin.Context) {
// 	type belanja struct {
// 		Item        string  `json: "nama_item"`
// 		Quantity    int     `json: "jumlah_item"`
// 		HargaSatuan float64 `json: "hargaSatuan"`
// 	}
// 	var belanjaJSON belanja
// 	if err := c.Bind(&belanjaJSON); err != nil {
// 		fmt.Println("Telah terjadi error")
// 	}
// 	listbelanjaan.belanjaStruct =
// 		append(listbelanjaan.belanjaStruct,
// 			belanjaStruct{
// 				belanjaJSON.Item,
// 				belanjaJSON.Quantity,
// 				belanjaJSON.HargaSatuan})
// 	fmt.Println(listbelanjaan.belanjaStruct)
// 	c.JSON(200, gin.H{
// 		"message": "success",
// 		"data": map[string]interface{}{
// 			"nama_item":   belanjaJSON.Item,
// 			"jumlah_item": belanjaJSON.Quantity,
// 			"hargaSatuan": belanjaJSON.HargaSatuan,
// 		},
// 	})
// }

// //update
// func (listbelanjaan *BelanjaanList) PutUpdatePurchase(c *gin.Context) {
// 	type belanja struct {
// 		Item        string  `json: "nama_item"`
// 		Quantity    int     `json: "jumlah_item"`
// 		HargaSatuan float64 `json: "hargaSatuan"`
// 	}
// 	var belanjaJSON belanja
// 	if err := c.Bind(&belanjaJSON); err != nil {
// 		fmt.Println("Telah terjadi error")
// 	}
// 	for i, v := range listbelanjaan.belanjaStruct {
// 		if v.Item == belanjaJSON.Item {
// 			listbelanjaan.belanjaStruct[i].Item = belanjaJSON.Item
// 			listbelanjaan.belanjaStruct[i].Quantity = belanjaJSON.Quantity
// 			listbelanjaan.belanjaStruct[i].HargaSatuan = belanjaJSON.HargaSatuan
// 		}
// 	}
// 	fmt.Println(listbelanjaan.belanjaStruct)
// 	c.JSON(200, gin.H{
// 		"message": "success",
// 		"data": map[string]interface{}{
// 			"nama_item":   belanjaJSON.Item,
// 			"jumlah_item": belanjaJSON.Quantity,
// 			"hargaSatuan": belanjaJSON.HargaSatuan,
// 		},
// 	})
// }

// //funct get data
// func (listbelanjaan *BelanjaanList) GetBelanjaan(c *gin.Context) {

// 	c.JSON(200, gin.H{
// 		"data": list.belanjaStruct,
// 	})
// }

// //getTotal
// func (listbelanjaan *BelanjaanList) GetTotalPurchase(c *gin.Context) {
// 	c.JSON(200, gin.H{
// 		"": "",
// 	})
// }
