package controller

import "github.com/gin-gonic/gin"

//responseAPI
func ResponseAPI(str interface{}, length int) gin.H {
	return gin.H{
		"message": "success",
		"data":    nil,
		"count":   length,
	}
}

//nil
func ResponseAPINil(str interface{}, length int) gin.H {
	return gin.H{
		"message": "success",
		"data":    nil,
		"count":   length,
	}
}
