package controller

import (
	"fmt"
	"net/http"
	"project/model"

	"github.com/gin-gonic/gin"
)

//PostAddProvince to DB Postgree
func (strDB *StrDB) PostAddProvince(c *gin.Context) {
	var (
		province model.Provinces
		result   gin.H
	)

	if err := c.Bind(&province); err != nil {
		fmt.Println("Tidak ada data atau terjadi kesalahan")
	} else {
		strDB.DB.Create(&province)
		result = gin.H{
			"message":  "success",
			"province": province.Name,
			"data": map[string]interface{}{
				"id":         province.ID,
				"name":       province.Name,
				"created_at": province.CreatedAt,
			},
		}
	}
	c.JSON(http.StatusOK, result)
}

//get
func (strDB *StrDB) GetListProvince(c *gin.Context) {
	var (
		province []model.Provinces
		result   gin.H
	)
	strDB.DB.Find(&province)
	if length := len(province); length <= 0 {
		// result = gin.H{
		// 	"message":"success",
		// 	"data":nil,
		// 	"count":length,
		// }
		result = ResponseAPINil(province, length)
	} else {
		result = ResponseAPI(province, length)
	}
	c.JSON(http.StatusOK, result)
}

//put
func (strDB *StrDB) PutUpdateProvince(c *gin.Context) {
	id := c.Query("id")
	name := c.PostForm("name")
	var (
		province    model.Provinces
		newProvince model.Provinces
		result      gin.H
	)
	err := strDB.DB.First(&province, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newProvince.Name = name
	err = strDB.DB.Model(&province).Updates(newProvince).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

//delete
func (strDB *StrDB) DeleteProvince(c *gin.Context) {
	var (
		province model.Provinces
		result   gin.H
	)
	id := c.Param("id")
	err := strDB.DB.First(&province, id).Error
	// err := idb.DB.First(&province, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = strDB.DB.Delete(&province).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}
