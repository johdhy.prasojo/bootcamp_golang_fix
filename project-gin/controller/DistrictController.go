package controller

import (
	"fmt"
	"net/http"
	"project/model"

	"github.com/gin-gonic/gin"
)

//PostAddProvince to DB Postgree
func (strDB *StrDB) PostAddDistrict(c *gin.Context) {
	var (
		district model.Districts
		result   gin.H
	)

	// name := c.PostForm("name")
	// idProvinsi := c.PostForm("id_Provinsi")
	// district.Name = name
	// district.Id_Provinces = idProvinsi

	// strDB.DB.Create(&district)
	// result = gin.H{
	// 	"result": district,
	// }
	// c.JSON(http.StatusOK, result)

	if err := c.Bind(&district); err != nil {
		fmt.Println("Tidak ada data atau terjadi kesalahan")
	} else {
		strDB.DB.Create(&district)
		result = gin.H{
			"message":     "success",
			"name":        district.Name,
			"id_provinsi": district.Id_Provinces,
			"data": map[string]interface{}{
				"id":          district.ID,
				"name":        district.Name,
				"id_provinsi": district.Id_Provinces,
				"created_at":  district.CreatedAt,
			},
		}
	}
	c.JSON(http.StatusOK, result)
}

//get
func (strDB *StrDB) GetDistrictProvince(c *gin.Context) {
	var (
		district []model.Districts
		result   gin.H
	)
	strDB.DB.Find(&district)
	if length := len(district); length <= 0 {
		// result = gin.H{
		// 	"message":"success",
		// 	"data":nil,
		// 	"count":length,
		// }
		result = ResponseAPINil(district, length)
	} else {
		result = ResponseAPI(district, length)
	}
	c.JSON(http.StatusOK, result)
}
