package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

//func Post
func (ListBelanjaan *BelanjaanList) PostBelanjaan(c *gin.Context) {
	var barang Belanjaan
	NamaBarang := c.PostForm("nama_item")
	JumlahItem := c.PostForm("jumlah_item")
	HargaSatuan := c.PostForm("hargaSatuan")

	barang.Item = NamaBarang
	barang.Quantity = JumlahItem
	barang.HargaSatuan = HargaSatuan
	ListBelanjaan.belanjaanBarang = append(ListBelanjaan.belanjaanBarang, barang)

	if err := c.Bind(&barang); err != nil {
		fmt.Println("Telah terjadi error")
	}

	c.JSON(http.StatusOK, gin.H{
		"message":     "success",
		"nama_item":   NamaBarang,
		"jumlah_item": JumlahItem,
		"hargaSatuan": HargaSatuan,
	})
}

//PUT
func (ListBelanjaan *BelanjaanList) PutUpdatePurchase(c *gin.Context) {
	type belanja struct {
		Item        string `json: "nama_item"`
		Quantity    string `json: "jumlah_item"`
		HargaSatuan string `json: "hargaSatuan"`
	}
	var belanjaJSON belanja
	if err := c.Bind(&belanjaJSON); err != nil {
		fmt.Println("Telah terjadi error")
	}
	for i, v := range ListBelanjaan.belanjaanBarang {
		if v.Item == belanjaJSON.Item {
			ListBelanjaan.belanjaanBarang[i].Item = belanjaJSON.Item
			ListBelanjaan.belanjaanBarang[i].Quantity = belanjaJSON.Quantity
			ListBelanjaan.belanjaanBarang[i].HargaSatuan = belanjaJSON.HargaSatuan
		}
	}

	fmt.Println(ListBelanjaan.belanjaanBarang)
	c.JSON(200, gin.H{
		"message": "success",
		"data": map[string]interface{}{
			"nama_item":   belanjaJSON.Item,
			"jumlah_item": belanjaJSON.Quantity,
			"hargaSatuan": belanjaJSON.HargaSatuan,
		},
	})
}

//funct get
func (ListBelanjaan *BelanjaanList) GetBelanjaan(c *gin.Context) {
	c.JSON(200, gin.H{
		"data": ListBelanjaan.belanjaanBarang,
	})
}

//getTotal
func (ListBelanjaan *BelanjaanList) GetTotalPurchase(c *gin.Context) {
	c.JSON(200, gin.H{
		"": "",
	})
}

// //coba get
// func GetData(c *gin.Context) {
// 	blj := model.NewBelanjaan()
// 	// databelanja := json.Marshal(blj)
// 	c.JSON(http.StatusOK, blj)
// }

// //func get by id
// func GetBelanjaanById(c *gin.Context) {
// 	blj := model.NewBelanjaan()

// 	// if err := blj.Where("id = ?", c.Param("id")).First(&book).Error; err != nil {
// 	// 	c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
// 	// 	return
// 	//   }

// 	c.JSON(http.StatusOK, gin.H{"data": blj})
// }

// // func UpdateData(c *gin.Context) {
// // 	blj := model.NewBelanjaan()
// // 	// Validate input
// // 	upd := model.UpdateBelanjaan()
// // 	if err := c.ShouldBindJSON(&upd); err != nil {
// // 		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
// // 		return
// // 	}
// // 	models.DB.Model(&blj).Updates(upd)

// // 	c.JSON(http.StatusOK, gin.H{"data": upd})
// // }

// // func Total(c *gin.Context) {
// // 	diskon := 10
// // 	var belanjaan Belanjaan
// // 	belanjaan.Id = 1
// // 	belanjaan.Item = "tas"
// // 	belanjaan.Quantity = 12
// // 	belanjaan.HargaSatuan = 1000

// // 	total := ()

// // }
