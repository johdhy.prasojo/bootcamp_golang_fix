package controller

//struct
type Belanjaan struct {
	Item        string  `json: "nama_item"`
	Quantity    string  `json: "jumlah_item"`
	HargaSatuan string 	`json: "hargaSatuan"`
}

//list data
type BelanjaanList struct {
	belanjaanBarang []Belanjaan
}
