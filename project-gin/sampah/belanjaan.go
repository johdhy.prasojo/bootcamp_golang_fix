package model

//struct
type Belanjaan struct {
	Id          int    `json: "id"`
	Item        string `json: "nama_item"`
	Quantity    int    `json: "jumlah_item"`
	HargaSatuan int    `json: "hargaSatuan"`
}

//update
type UpBelanjaan struct {
	Id          int    `json: "id"`
	Item        string `json: "nama_item"`
	Quantity    int    `json: "jumlah_item"`
	HargaSatuan int    `json: "hargaSatuan"`
}

type Total struct {
	Diskon        int
	TotalPotongan int
}

// data
func NewBelanjaan() []Belanjaan {
	blj := []Belanjaan{
		Belanjaan{
			Id:          1,
			Item:        "soundsysten",
			Quantity:    10,
			HargaSatuan: 1000,
		},
		Belanjaan{
			Id:          2,
			Item:        "laptop",
			Quantity:    20,
			HargaSatuan: 2000,
		},
		Belanjaan{
			Id:          3,
			Item:        "pc",
			Quantity:    30,
			HargaSatuan: 3000,
		},
	}
	return blj
}

//udpate func
func UpdateBelanjaan() []UpBelanjaan {
	upd := []UpBelanjaan{
		UpBelanjaan{
			Id:          1,
			Item:        "kursi",
			Quantity:    10,
			HargaSatuan: 1000,
		},
		UpBelanjaan{
			Id:          2,
			Item:        "meja",
			Quantity:    20,
			HargaSatuan: 2000,
		},
		UpBelanjaan{
			Id:          3,
			Item:        "kasur",
			Quantity:    30,
			HargaSatuan: 3000,
		},
	}
	return upd
}

// import (
// 	"github.com/jinzhu/gorm"
// 	_ "github.com/jinzhu/gorm/dialects/sqlite"
// )

// var DB *gorm.DB

// type Belanjaan struct {
// 	Id          uint    `json: "id"` gorm:"primary_key"`
// 	Item        string `json: "nama_item"`
// 	Quantity    int    `json: "jumlah_item"`
// 	HargaSatuan int    `json: "hargaSatuan"`
// }

// func ConnectDataBase() {
// 	database, err := gorm.Open("sqlite3", "test.db")

// 	if err != nil {
// 		panic("Failed to connect to database!")
// 	}

// 	database.AutoMigrate(&Belanjaan{})

// 	DB = database
// }
